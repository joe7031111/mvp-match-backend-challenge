const express = require("express");
const app = express();
const bcrypt = require("bcrypt");
const userController = require("./controller/userController");
const productController = require("./controller/productController");
const migrateController = require("./controller/migrateController");
const depositController = require("./controller/depositController");
const resetController = require("./controller/resetController");
const buyController = require("./controller/buyController");
const auth = require("./JWT/middleware");
const jwt = require("jsonwebtoken");

app.use(express.json());

console.log("Check: Ok");

const users = [];
//show all user
app.get("/users", auth.sellerMiddleware, userController.getAllUsers);

//register a new user
app.post("/users", userController.registerUsers);

//login user with username and password
app.post("/users/login", userController.loginUser);

//Update user by passing the id
app.post("/users/update/:id", auth.sellerMiddleware, userController.updateUser);

//Delete user by passing the id
app.delete("/users/delete/:id", auth.sellerMiddleware, userController.deleteUser);

// create table if not Exists
app.get("/createTableIfNotExists", migrateController.createNewTableIfNotExists);


//show all products only seller
app.get("/products", auth.sellerMiddleware, productController.getAllProducts);

//register a new product only seller
app.post("/products", auth.sellerMiddleware, productController.createProduct);

//Update product by passing the id only seller
app.post("/product/update/:id", auth.sellerMiddleware, productController.updateProduct);

//Delete product by passing the id
app.delete("/product/delete/:id", auth.sellerMiddleware, productController.deleteProduct);


//endpoint deposit
app.post("/deposit/:id", auth.buyerMiddleware, depositController.insertCoin);


//endpoint buy
app.post("/buy/:id", auth.buyerMiddleware, buyController.buyProduct);

//endpoint reset
app.post("/reset", auth.buyerMiddleware, resetController.reset);


app.listen(3000);




