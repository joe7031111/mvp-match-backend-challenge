const mysql = require("mysql2/promise");

// Connect to the database
async function createConnection() {
  const connection = await mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root",
    database: "mvpDB",
    port: "3307",
  });

  connection.connect(function (error) {
    if (error) {
      console.error('Error connecting to database: ' + error.stack);
      return;
    }

    console.log('Connected to database with threadId: ' + connection.threadId);
  });

  return connection;
}

module.exports = createConnection;


