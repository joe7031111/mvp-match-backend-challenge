const jwt = require("jsonwebtoken");
const secretKey = process.env.ACCESS_TOKEN_SECRET;

function sellerMiddleware(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(401);

  jwt.verify(token, secretKey, (err, user) => {
    if (err || user.role !== "seller") return res.sendStatus(403);
    req.user = user;
    next();
  });
}

function buyerMiddleware(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(401);

  jwt.verify(token, secretKey, (err, user) => {
    if (err || user.role !== "buyer") return res.sendStatus(403);
    req.user = user;
    next();
  });
}

module.exports = {
  sellerMiddleware,
  buyerMiddleware,
};
