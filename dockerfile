FROM node:14-alpine
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 3000
# CMD [ "npm", "run", "devStart" && "docker-compose", "logs" ]

# Dieses Dockerfile verwendet node:14-alpine als Basisimage, 
# erstellt ein Arbeitsverzeichnis /app, kopiert package.json 
# und package-lock.json in das Arbeitsverzeichnis und führt npm 
# install aus. Dann kopiert es alle anderen Dateien in das Arbeitsverzeichnis, 
# öffnet den Port 3000 und startet die Anwendung mit npm start.
