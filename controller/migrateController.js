require('dotenv').config();
const express = require("express");
const mysql = require('mysql2/promise');
const app = express();
const createConnection = require("../db_conn");

app.use(express.json());
// const connection = createConnection();

// create user table
async function createTableIfNotExistsUserTable(tableName) {

    const connection = await createConnection();
    try {
        // Check if table exists
        const [rows, fields] = await connection.execute(`SHOW TABLES LIKE '${tableName}'`);
        if (rows.length > 0) {
            console.log(`Table '${tableName}' already exists.`);
            return;
        }

        // Create table if it does not exist
        const [results, _] = await connection.execute(`
      CREATE TABLE ${tableName} (
        id INT NOT NULL AUTO_INCREMENT,
        username VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL,
        deposit VARCHAR(255) NOT NULL,
        role VARCHAR(255) NOT NULL,
        createt_at datetime(6) NOT NULL,
        PRIMARY KEY (id)
      )
    `);

        console.log(`Table '${tableName}' created successfully.`);
    } catch (error) {
        console.error(`Error creating table '${tableName}': `, error);
    } finally {
        connection.end();
    }
}

//create product table
async function createTableIfNotExistsProductTable(tableName) {

    const connection = await createConnection();
    try {
        // Check if table exists
        const [rows, fields] = await connection.execute(`SHOW TABLES LIKE '${tableName}'`);
        if (rows.length > 0) {
            console.log(`Table '${tableName}' already exists.`);
            return;
        }

        // Create table if it does not exist
        const [results, _] = await connection.execute(`
      CREATE TABLE ${tableName} (
        id INT NOT NULL AUTO_INCREMENT,
        productName VARCHAR(255) NOT NULL,
        amountAvailable int NOT NULL,
        cost int NOT NULL,
        sellerId int NOT NULL,
        createt_at datetime(6) NOT NULL,
        PRIMARY KEY (id)
      )
    `);

        console.log(`Table '${tableName}' created successfully.`);
    } catch (error) {
        console.error(`Error creating table '${tableName}': `, error);
    } finally {
        connection.end();
    }
}

function createNewTableIfNotExists(req, res) {
    createTableIfNotExistsUserTable('user');
    createTableIfNotExistsProductTable('product');
    res.status(200).send('createNewTableIfNotExists');
};

module.exports = {
    createNewTableIfNotExists,
    createTableIfNotExistsUserTable,
    createTableIfNotExistsProductTable
};
