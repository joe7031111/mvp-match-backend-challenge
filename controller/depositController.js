require("dotenv").config();
const express = require("express");
const mysql = require("mysql2/promise");
const app = express();
const createConnection = require("../db_conn");
const secretKey = process.env.ACCESS_TOKEN_SECRET;
const jwt = require("jsonwebtoken");

app.use(express.json());

let coinValueDeposit = 0;


async function insertCoin(req, res) {
  const coinValues = [5, 10, 20, 50, 100];
  const coinValue = parseInt(req.params.id);

  coinValueDeposit += coinValue;
  console.log(coinValueDeposit);
  // Check if the value of the coin is allowed
  if (!coinValues.includes(coinValue)) {
    return res
      .status(400)
      .send(
        "Invalid coin value. Coins can only have the value 5, 10, 20, 50, 100!"
      );
  } else {
    const token = await req.headers.authorization.split(" ")[1]; // get token from headers
    const decodedToken = await jwt.verify(token, secretKey); // decode token with secret key
    const sellerId = await decodedToken.userId; // get userId from decoded token

    const connection = await createConnection();


    try {
      const [result] = await connection.query(
        "UPDATE user SET deposit = ? WHERE id = ?",
        [coinValueDeposit, sellerId]
      );
      if (result.affectedRows === 0) {
        return res.status(404).send("User not found");
      }
      return res
        .status(200)
        .send(
          `Successfully deposited coin. Your account balance is ${coinValueDeposit} cent`
        );
    } catch (error) {
      console.error(error);
      res.status(500).send("Error updating Deposit");
    } finally {
      connection.end();
    }
  }
}

function resetCoinValueDeposit(){
  coinValueDeposit = 0;
};

module.exports = {
  insertCoin,
  resetCoinValueDeposit,
};
