require("dotenv").config();
const express = require("express");
const mysql = require("mysql2/promise");
const app = express();
const createConnection = require("../db_conn");
const secretKey = process.env.ACCESS_TOKEN_SECRET;
const jwt = require("jsonwebtoken");
const depositController = require("../controller/depositController");

app.use(express.json());

let changeDeposit;
let productCost;
let currentDeposit;

async function buyProduct(req, res) {
  //fetch product from db by productId

  const token = await req.headers.authorization.split(" ")[1]; // get token from headers
  const decodedToken = await jwt.verify(token, secretKey); // decode token with secret key
  const sellerId = await decodedToken.userId; // get userId from decoded token
  const connection = await createConnection();
  try {
    // get product with id
    const [product] = await connection.query(
      "SELECT * FROM product where id = ?",
      [req.params.id]
    );
    console.log(product);
    productCost = product[0].cost;
    console.log(productCost);

    // get the deposit from user with id
    const [userDeposit] = await connection.query(
      "SELECT deposit FROM user where id = ?",
      [sellerId]
    );
    currentDeposit = userDeposit[0].deposit;
    console.log(currentDeposit);
  } catch (error) {
    console.error("Error during query: " + error.stack);
    res.status(500).json({ error: "Internal Server Error" });
  } finally {
    connection.end();
  }
  //   }
  // todo get current deposit value and calculate deposit minus product cost
  if (currentDeposit >= productCost) {
    changeDeposit = currentDeposit - productCost;
    const moneyBack = await calculateChange(changeDeposit, validCoins);

    res
      .status(200)
      .send(
        `Your change is: ${changeDeposit} cents, which you receive in ${moneyBack}`
      );

    reset(sellerId);
    currentDeposit = 0;
  } else {
    res.status(401).send("You have to put in more money");
  }
}
// return the rest of the deposit in allowable coins 5, 10, 20 ,50 ,100 as array

const validCoins = [5, 10, 20, 50, 100];

async function calculateChange(changeDeposit, validCoins) {
  const moneyBack = [];
  for (let i = validCoins.length - 1; i >= 0; i--) {
    const coin = validCoins[i];

    while (changeDeposit >= coin) {
      moneyBack.push(coin);
      changeDeposit -= coin;
    }
  }
  console.log("moneyBack", moneyBack);

  return moneyBack;
}

//reset deposit
async function reset(sellerId) {
    const connection = await createConnection();
    try {
        console.log(depositController.coinValueDeposit);
        connection.query("UPDATE user SET deposit = 0 WHERE id = ?", [sellerId]);
        depositController.resetCoinValueDeposit();
        console.log(depositController.coinValueDeposit);
        
    }  catch (error) {
        console.error("Error during query: " + error.stack);
        res.status(500).json({ error: "Internal Server Error" });
      } finally {
        connection.end();
      }
}

module.exports = {
  buyProduct,
};
