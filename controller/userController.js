require("dotenv").config();
const express = require("express");
const app = express();
const createConnection = require("../db_conn");
const secretKey = process.env.ACCESS_TOKEN_SECRET;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

app.use(express.json());


async function getAllUsers(req, res) {
  const connection = await createConnection();

  try {
    const [results] = await connection.query("SELECT * FROM user");
    console.log(results);
    res.status(200).json(results);
  } catch (error) {
    console.error("Error during query: " + error.stack);
    res.status(500).json({ error: "Internal Server Error" });
  }
  finally {
    connection.end();
  }
}

//postUser
async function registerUsers(req, res) {
  try {

    const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const username = req.body.username;
    const password = hashedPassword;
    const deposit = req.body.deposit;
    const role = req.body.role;
    const createt_at = new Date();
    console.log(password);
    const connection = await createConnection();

    try {
      await connection.query(
        "INSERT INTO user (username, password, deposit, role, createt_at) VALUES (?, ?, ?, ?,?)",
        [username, hashedPassword, deposit, role, createt_at]
      );
      res.status(201).send("New user created");
    } catch (error) {
      console.error(error);
      res.status(500).send();
    }
    finally {
      connection.end();
    }
  } catch (error) {
    console.error(error);
    res.status(500).send();
  }
}

//login
async function loginUser(req, res) {
  const connection = await createConnection();

  const username = req.body.username;
  try {
    var sql = "SELECT * FROM user WHERE username = ?";
    const [userFromDB] = await connection.query(
      sql,
      [username],
      function (err, result) {
        if (err) throw err;
        res.status(201).send();
      }
    );
    // Check if user exists in the database
    const user = userFromDB[0].password;
    if (user == null) {
      return res.status(400).send("cannot find user");
    }

    if (await bcrypt.compare(req.body.password, user)) {
      // Check user's role and set token and middleware accordingly
      const role = userFromDB[0].role;

      let accessToken;
      try {
        if (role === "seller") {
          console.log("seller");
          accessToken = jwt.sign(
            {
              userId: userFromDB[0].id,
              username: userFromDB[0].username,
              role: "seller",
            },
            secretKey
          );
        } else if (role === "buyer") {
          console.log("buyer");
          accessToken = jwt.sign(
            {
              userId: userFromDB[0].id,
              username: userFromDB[0].username,
              role: "buyer",
            },
            secretKey
          );
        } else {
          console.log("else");
          return res.status(400).send("Unknown user role");
        }

        res.status(201).send({
          message: "success",
          accessToken: accessToken,
        });
      } catch (error) {
        console.log(error);
      }
    }
  } catch (error) {
    res.status(501).send();
  }
  finally {
    connection.end();
  }
}

//Update user
async function updateUser(req, res) {
  const { id } = req.params;
  const { username, deposit, role } = req.body;
  const hashedPassword = await bcrypt.hash(req.body.password, 10);


  const connection = await createConnection();
  try {
    const [result] = await connection.query(
      "UPDATE user SET username = ?, password = ?, deposit = ?, role = ? WHERE id = ?",
      [username, hashedPassword, deposit, role, id]
    );
    if (result.affectedRows === 0) {
      return res.status(404).send("User not found");
    }
    res.status(200).send("User updated successfully");
  } catch (error) {
    console.error(error);
    res.status(500).send("Error updating user");
  }
  finally {
    connection.end();
  }
}

//Delete user
async function deleteUser(req, res) {
  const connection = await createConnection();
  const userId = req.params.id;
  console.log(userId);


  try {
    const [result] = await connection.query("DELETE FROM user WHERE id = ?", [
      userId,
    ]);

    if (result.affectedRows === 0) {
      return res.status(404).send("User not found");
    }

    res.status(200).send("User deleted");
  } catch (error) {
    console.error("Error during query: " + error.stack);
    res.status(500).send();
  }
  finally {
    connection.end();
  }
}


module.exports = {
  getAllUsers,
  registerUsers,
  loginUser,
  updateUser,
  deleteUser,
};
