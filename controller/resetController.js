require("dotenv").config();
const express = require("express");
const mysql = require("mysql2/promise");
const app = express();
const createConnection = require("../db_conn");
const secretKey = process.env.ACCESS_TOKEN_SECRET;
const jwt = require("jsonwebtoken");
const depositController = require("../controller/depositController");

app.use(express.json());

//reset deposit
async function reset(req, res) {
  const connection = await createConnection();

  try {
    const token = await req.headers.authorization.split(" ")[1]; // get token from headers
    const decodedToken = await jwt.verify(token, secretKey); // decode token with secret key
    const sellerId = await decodedToken.userId; // get userId from decoded token
    console.log(sellerId);
    connection.query("UPDATE user SET deposit = 0 WHERE id = ?", [sellerId]);

    depositController.resetCoinValueDeposit();
    res.status(200).send("Reset success");
  } catch (error) {
    console.log(error);
  } finally {
    connection.end();
  }
}

module.exports = {
  reset,
};
