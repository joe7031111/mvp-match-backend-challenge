require("dotenv").config();
const express = require("express");
const mysql = require("mysql2/promise");
const app = express();
const createConnection = require("../db_conn");
const secretKey = process.env.ACCESS_TOKEN_SECRET;
const middleware = require("../JWT/middleware");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

app.use(express.json());

async function getAllProducts(req, res) {


  try {
    const [results] = await connection.query("SELECT * FROM product");
    console.log(results);
    res.status(200).json(results);
  } catch (error) {
    console.error("Error during query: " + error.stack);
    res.status(500).json({ error: "Internal Server Error" });
  } finally {
    connection.end();
  }
}

//create product
async function createProduct(req, res) {
  try {
    const productName = req.body.productName;
    const amountAvailable = req.body.amountAvailable;
    const cost = req.body.cost;

    const token = await req.headers.authorization.split(" ")[1]; // get token from headers
    const decodedToken = await jwt.verify(token, secretKey); // decode token with secret key
    const sellerId = await decodedToken.userId; // get userId from decoded token
    const createt_at = new Date();

    const connection = await createConnection();
    try {
      await connection.query(
        "INSERT INTO product (productName, amountAvailable, cost, sellerId, createt_at) VALUES (?, ?, ?, ?,?)",
        [productName, amountAvailable, cost, sellerId, createt_at]
      );
      res.status(201).send(`New Product '${productName}' created`);
    } catch (error) {
      console.error(error);
      res.status(500).send();
    } finally {
      connection.end();
    }
  }
  catch (error) {
    console.error(error);
    res.status(500).send();
  }
}

//Update product
async function updateProduct(req, res) {
  const { id } = req.params;
  const { productName, amountAvailable, cost } = req.body;
  const token = await req.headers.authorization.split(" ")[1]; // get token from headers
  const decodedToken = await jwt.verify(token, secretKey); // decode token with secret key
  const sellerId = await decodedToken.userId; // get userId from decoded token


  const connection = await createConnection();
  try {
    const [result] = await connection.query(
      "UPDATE product SET productName = ?, amountAvailable = ?, cost = ?, sellerId = ? WHERE id = ?",
      [productName, amountAvailable, cost, sellerId, id]
    );
    if (result.affectedRows === 0) {
      return res.status(404).send("Product not found");
    }
    res.status(200).send("Product updated successfully");
  } catch (error) {
    console.error(error);
    res.status(500).send("Error updating Product");
  } finally {
    connection.end();
  }
}

//Delete product
async function deleteProduct(req, res) {
  const productId = req.params.id;


  const connection = await createConnection();
  try {
    const [result] = await connection.query(
      "DELETE FROM product WHERE id = ?",
      [productId]
    );

    if (result.affectedRows === 0) {
      return res.status(404).send("Product not found");
    }
    res.status(200).send(`Product with id: ${productId} deleted successfull`);

  } catch (error) {
    console.error("Error during query: " + error.stack);
    res.status(500).send();
  } finally {
    connection.end();
  }
}


module.exports = {
  getAllProducts,
  createProduct,
  updateProduct,
  deleteProduct,
};
